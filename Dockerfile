################################################################################
#
# docker build -t jontow/alpine-nginx-static:v0 .
# docker run -it -u 10002000:0 jontow/alpine-nginx-static:v0
#
################################################################################

FROM docker.io/alpine:latest
MAINTAINER Jonathan Towne <jontow@unc.edu>

# These labels are used by OpenShift in order to display information inside the project
LABEL io.k8s.description="nginx on alpine hosting a static site" \
      io.k8s.display-name="alpine-nginx-static" \
      io.openshift.expose-services="8080:tcp"

# Install and configure nginx
RUN apk add nginx && \
    chgrp -R 0 /var/log/nginx && \
    chmod -R g=u /var/log/nginx && \
    chgrp -R 0 /var/lib/nginx && \
    chmod -R g=u /var/lib/nginx && \
    chgrp -R 0 /var/tmp/nginx && \
    chmod -R g=u /var/tmp/nginx && \
    chgrp 0 /run && \
    chmod g=u /run && \
    mkdir -p /run/nginx && \
    chgrp 0 /run/nginx && \
    chmod g=u /run/nginx && \
    rm /etc/nginx/conf.d/default.conf && \
    mkdir -p /www && \
    echo 'Welcome to alpine-nginx-static' >/www/index.html && \
    chgrp -R 0 /www && \
    chmod -R g=u /www

COPY nginx_static.conf /etc/nginx/conf.d/static.conf

EXPOSE 8080
ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]
